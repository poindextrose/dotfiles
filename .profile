set -o vi

source ~/git/dotfiles/git-completion.bash
source ~/git/dotfiles/git-prompt.sh

PROMPT_COMMAND='__git_ps1 "\u@\h:\w" "\\\$ "'

# add color to ls
alias ls='ls -GFh'
